
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
<<<<<<< HEAD
-- Date Created: 12/14/2017 19:46:50
-- Generated from EDMX file: C:\Users\Jirattikan Sopang\Documents\twitter3\twitter3\tweett\Models\ModelTW.edmx
=======
-- Date Created: 12/14/2017 19:35:18
-- Generated from EDMX file: C:\Users\Jirattikan Sopang\Desktop\twitter\tweett\Models\ModelTW.edmx
>>>>>>> 8ca85eb950ffac674af1ea6bc81c5617634836b6
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
<<<<<<< HEAD
USE [Tw];
=======
USE [TWDB];
>>>>>>> 8ca85eb950ffac674af1ea6bc81c5617634836b6
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_AccountRetweet_Account]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AccountRetweet] DROP CONSTRAINT [FK_AccountRetweet_Account];
GO
IF OBJECT_ID(N'[dbo].[FK_AccountRetweet_Retweet]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AccountRetweet] DROP CONSTRAINT [FK_AccountRetweet_Retweet];
GO
IF OBJECT_ID(N'[dbo].[FK_RetweetPosts]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[RetweetSet] DROP CONSTRAINT [FK_RetweetPosts];
GO
IF OBJECT_ID(N'[dbo].[FK_PostsHashtag_Posts]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PostsHashtag] DROP CONSTRAINT [FK_PostsHashtag_Posts];
GO
IF OBJECT_ID(N'[dbo].[FK_PostsHashtag_Hashtag]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PostsHashtag] DROP CONSTRAINT [FK_PostsHashtag_Hashtag];
GO
IF OBJECT_ID(N'[dbo].[FK_AccountMessage]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MessageSet] DROP CONSTRAINT [FK_AccountMessage];
GO
IF OBJECT_ID(N'[dbo].[FK_PostsReply_Posts]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PostsReply] DROP CONSTRAINT [FK_PostsReply_Posts];
GO
IF OBJECT_ID(N'[dbo].[FK_PostsReply_Reply]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PostsReply] DROP CONSTRAINT [FK_PostsReply_Reply];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AccountSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AccountSet];
GO
IF OBJECT_ID(N'[dbo].[RetweetSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RetweetSet];
GO
IF OBJECT_ID(N'[dbo].[PostsSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PostsSet];
GO
IF OBJECT_ID(N'[dbo].[MessageSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MessageSet];
GO
IF OBJECT_ID(N'[dbo].[HashtagSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HashtagSet];
GO
IF OBJECT_ID(N'[dbo].[ReplySet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ReplySet];
GO
IF OBJECT_ID(N'[dbo].[AccountRetweet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AccountRetweet];
GO
IF OBJECT_ID(N'[dbo].[PostsHashtag]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PostsHashtag];
GO
IF OBJECT_ID(N'[dbo].[PostsReply]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PostsReply];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'AccountSet'
CREATE TABLE [dbo].[AccountSet] (
    [IdUser] int IDENTITY(1,1) NOT NULL,
    [username] nvarchar(max)  NOT NULL,
    [password] nvarchar(max)  NOT NULL,
    [email] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'RetweetSet'
CREATE TABLE [dbo].[RetweetSet] (
    [IdRet] int IDENTITY(1,1) NOT NULL,
    [keep_status] nvarchar(max)  NOT NULL,
    [count_R] nvarchar(max)  NOT NULL,
    [PostIdPost] nvarchar(max)  NOT NULL,
    [Postse_IdPost] int  NOT NULL
);
GO

-- Creating table 'PostsSet'
CREATE TABLE [dbo].[PostsSet] (
    [IdPost] int IDENTITY(1,1) NOT NULL,
    [textP] nvarchar(max)  NOT NULL,
    [date_timeP] nvarchar(max)  NOT NULL,
    [AccountIdUser] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'MessageSet'
CREATE TABLE [dbo].[MessageSet] (
    [Idmes] int IDENTITY(1,1) NOT NULL,
    [textM] nvarchar(max)  NOT NULL,
    [date_timeM] nvarchar(max)  NOT NULL,
    [AccountIdUser] int  NOT NULL,
    [AccountIdUser1] int  NOT NULL,
    [AccountIdUser2] int  NOT NULL
);
GO

-- Creating table 'HashtagSet'
CREATE TABLE [dbo].[HashtagSet] (
    [IdHash] int IDENTITY(1,1) NOT NULL,
    [create_hashtag] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ReplySet'
CREATE TABLE [dbo].[ReplySet] (
    [IdReply] int IDENTITY(1,1) NOT NULL,
    [textR] nvarchar(max)  NOT NULL,
    [date_time] nvarchar(max)  NOT NULL,
    [PostIdPost] nvarchar(max)  NOT NULL,
    [AccountIdUser] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'AccountRetweet'
CREATE TABLE [dbo].[AccountRetweet] (
    [Accounts_IdUser] int  NOT NULL,
    [Retweets_IdRet] int  NOT NULL
);
GO

-- Creating table 'PostsHashtag'
CREATE TABLE [dbo].[PostsHashtag] (
    [Postse_IdPost] int  NOT NULL,
    [Hashtags_IdHash] int  NOT NULL
);
GO

-- Creating table 'PostsReply'
CREATE TABLE [dbo].[PostsReply] (
    [Posts_IdPost] int  NOT NULL,
    [Reply_IdReply] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [IdUser] in table 'AccountSet'
ALTER TABLE [dbo].[AccountSet]
ADD CONSTRAINT [PK_AccountSet]
    PRIMARY KEY CLUSTERED ([IdUser] ASC);
GO

-- Creating primary key on [IdRet] in table 'RetweetSet'
ALTER TABLE [dbo].[RetweetSet]
ADD CONSTRAINT [PK_RetweetSet]
    PRIMARY KEY CLUSTERED ([IdRet] ASC);
GO

-- Creating primary key on [IdPost] in table 'PostsSet'
ALTER TABLE [dbo].[PostsSet]
ADD CONSTRAINT [PK_PostsSet]
    PRIMARY KEY CLUSTERED ([IdPost] ASC);
GO

-- Creating primary key on [Idmes] in table 'MessageSet'
ALTER TABLE [dbo].[MessageSet]
ADD CONSTRAINT [PK_MessageSet]
    PRIMARY KEY CLUSTERED ([Idmes] ASC);
GO

-- Creating primary key on [IdHash] in table 'HashtagSet'
ALTER TABLE [dbo].[HashtagSet]
ADD CONSTRAINT [PK_HashtagSet]
    PRIMARY KEY CLUSTERED ([IdHash] ASC);
GO

-- Creating primary key on [IdReply] in table 'ReplySet'
ALTER TABLE [dbo].[ReplySet]
ADD CONSTRAINT [PK_ReplySet]
    PRIMARY KEY CLUSTERED ([IdReply] ASC);
GO

-- Creating primary key on [Accounts_IdUser], [Retweets_IdRet] in table 'AccountRetweet'
ALTER TABLE [dbo].[AccountRetweet]
ADD CONSTRAINT [PK_AccountRetweet]
    PRIMARY KEY CLUSTERED ([Accounts_IdUser], [Retweets_IdRet] ASC);
GO

-- Creating primary key on [Postse_IdPost], [Hashtags_IdHash] in table 'PostsHashtag'
ALTER TABLE [dbo].[PostsHashtag]
ADD CONSTRAINT [PK_PostsHashtag]
    PRIMARY KEY CLUSTERED ([Postse_IdPost], [Hashtags_IdHash] ASC);
GO

-- Creating primary key on [Posts_IdPost], [Reply_IdReply] in table 'PostsReply'
ALTER TABLE [dbo].[PostsReply]
ADD CONSTRAINT [PK_PostsReply]
    PRIMARY KEY CLUSTERED ([Posts_IdPost], [Reply_IdReply] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Accounts_IdUser] in table 'AccountRetweet'
ALTER TABLE [dbo].[AccountRetweet]
ADD CONSTRAINT [FK_AccountRetweet_Account]
    FOREIGN KEY ([Accounts_IdUser])
    REFERENCES [dbo].[AccountSet]
        ([IdUser])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Retweets_IdRet] in table 'AccountRetweet'
ALTER TABLE [dbo].[AccountRetweet]
ADD CONSTRAINT [FK_AccountRetweet_Retweet]
    FOREIGN KEY ([Retweets_IdRet])
    REFERENCES [dbo].[RetweetSet]
        ([IdRet])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountRetweet_Retweet'
CREATE INDEX [IX_FK_AccountRetweet_Retweet]
ON [dbo].[AccountRetweet]
    ([Retweets_IdRet]);
GO

-- Creating foreign key on [Postse_IdPost] in table 'RetweetSet'
ALTER TABLE [dbo].[RetweetSet]
ADD CONSTRAINT [FK_RetweetPosts]
    FOREIGN KEY ([Postse_IdPost])
    REFERENCES [dbo].[PostsSet]
        ([IdPost])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RetweetPosts'
CREATE INDEX [IX_FK_RetweetPosts]
ON [dbo].[RetweetSet]
    ([Postse_IdPost]);
GO

-- Creating foreign key on [Postse_IdPost] in table 'PostsHashtag'
ALTER TABLE [dbo].[PostsHashtag]
ADD CONSTRAINT [FK_PostsHashtag_Posts]
    FOREIGN KEY ([Postse_IdPost])
    REFERENCES [dbo].[PostsSet]
        ([IdPost])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Hashtags_IdHash] in table 'PostsHashtag'
ALTER TABLE [dbo].[PostsHashtag]
ADD CONSTRAINT [FK_PostsHashtag_Hashtag]
    FOREIGN KEY ([Hashtags_IdHash])
    REFERENCES [dbo].[HashtagSet]
        ([IdHash])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PostsHashtag_Hashtag'
CREATE INDEX [IX_FK_PostsHashtag_Hashtag]
ON [dbo].[PostsHashtag]
    ([Hashtags_IdHash]);
GO

-- Creating foreign key on [AccountIdUser2] in table 'MessageSet'
ALTER TABLE [dbo].[MessageSet]
ADD CONSTRAINT [FK_AccountMessage]
    FOREIGN KEY ([AccountIdUser2])
    REFERENCES [dbo].[AccountSet]
        ([IdUser])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AccountMessage'
CREATE INDEX [IX_FK_AccountMessage]
ON [dbo].[MessageSet]
    ([AccountIdUser2]);
GO

-- Creating foreign key on [Posts_IdPost] in table 'PostsReply'
ALTER TABLE [dbo].[PostsReply]
ADD CONSTRAINT [FK_PostsReply_Posts]
    FOREIGN KEY ([Posts_IdPost])
    REFERENCES [dbo].[PostsSet]
        ([IdPost])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Reply_IdReply] in table 'PostsReply'
ALTER TABLE [dbo].[PostsReply]
ADD CONSTRAINT [FK_PostsReply_Reply]
    FOREIGN KEY ([Reply_IdReply])
    REFERENCES [dbo].[ReplySet]
        ([IdReply])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PostsReply_Reply'
CREATE INDEX [IX_FK_PostsReply_Reply]
ON [dbo].[PostsReply]
    ([Reply_IdReply]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------