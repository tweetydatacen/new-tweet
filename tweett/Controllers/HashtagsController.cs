﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tweett.Models;

namespace tweett.Controllers
{
    public class HashtagsController : Controller
    {
        private Model1Container db = new Model1Container();

        // GET: Hashtags
        public ActionResult Index()
        {
            return View(db.HashtagSet.ToList());
        }

        // GET: Hashtags/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hashtag hashtag = db.HashtagSet.Find(id);
            List<Hashtag> hashtags = db.HashtagSet.ToList();
            ViewData["hashtags"] = hashtags;
            if (hashtag == null)
            {
                return HttpNotFound();
            }
            return View(hashtag);
        }

        // GET: Hashtags/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Hashtags/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdHash,create_hashtag")] Hashtag hashtag)
        {
            if (ModelState.IsValid)
            {
                db.HashtagSet.Add(hashtag);
                db.SaveChanges();
                return RedirectToAction("IndexPost","Posts");
            }

            return View(hashtag);
        }

        // GET: Hashtags/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hashtag hashtag = db.HashtagSet.Find(id);
            if (hashtag == null)
            {
                return HttpNotFound();
            }
            return View(hashtag);
        }

        // POST: Hashtags/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdHash,create_hashtag")] Hashtag hashtag)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hashtag).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(hashtag);
        }

        // GET: Hashtags/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hashtag hashtag = db.HashtagSet.Find(id);
            if (hashtag == null)
            {
                return HttpNotFound();
            }
            return View(hashtag);
        }

        // POST: Hashtags/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hashtag hashtag = db.HashtagSet.Find(id);
            db.HashtagSet.Remove(hashtag);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
