﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tweett.Models;

namespace tweett.Controllers
{
    public class PostsController : Controller
    {
        private Model1Container db = new Model1Container();

        // GET: Posts
        public ActionResult Index()
        {
            
            ViewData["Hashtags"] = db.HashtagSet.ToList();
            ViewData["posts"] = db.PostsSet.ToList();
            ViewData["account"] = db.AccountSet.ToList();
            ViewData["comment"] = db.ReplySet.ToList();
            if (Session["Username"] != null) {
                String user = Session["Username"].ToString();
                ViewData["User"] = db.AccountSet.FirstOrDefault(u => u.username == user);
            }
           
            return View();
        }

        // GET: Posts/Details/5
        public ActionResult Details(int? id)
        {
            ViewData["account"] = db.AccountSet.ToList();
            ViewData["Hashtags"] = db.HashtagSet.ToList();
            ViewData["account"] = db.AccountSet.ToList();
            ViewData["comment"] = db.ReplySet.ToList();

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           // dynamic mymodel = new ExpandoObject();
           // mymodel.Posts = db.PostsSet.Find(id);
           // mymodel.Reply = db.ReplySet.ToList();
           // mymodel.Hashtag = db.HashtagSet.ToList();
            Posts posts = db.PostsSet.Find(id);
            if (posts == null)
            {
                return HttpNotFound();
            }
            return View(posts);
        }
       


        // GET: Posts/Create
        public ActionResult Create()
        {
            return View();
        }

        

        // POST: Posts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index([Bind(Include = "IdPost,textP,date_timeP,AccountIdUser")] Posts posts)
        {
            if (ModelState.IsValid)
            {
                db.PostsSet.Add(posts);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(posts);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult comment([Bind(Include = "IdPost,textP,date_timeP,AccountIdUser")] Posts posts)
        {
            if (ModelState.IsValid)
            {
                db.PostsSet.Add(posts);
                db.SaveChanges();
                return RedirectToAction("Details");
            }

            return View(posts);
        }

        // GET: Posts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Posts posts = db.PostsSet.Find(id);
            if (posts == null)
            {
                return HttpNotFound();
            }
            return View(posts);
        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdPost,textP,date_timeP,AccountIdUser")] Posts posts)
        {
            if (ModelState.IsValid)
            {
                db.Entry(posts).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(posts);
        }

        // GET: Posts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Posts posts = db.PostsSet.Find(id);
            if (posts == null)
            {
                return HttpNotFound();
            }
            List<Reply> repliess = db.ReplySet.ToList();
            ViewData["repliess"] = repliess;
            return View(posts);
        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Posts posts = db.PostsSet.Find(id);
            db.PostsSet.Remove(posts);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
