﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using tweett.Models;

namespace tweett.Controllers
{
    public class AccountsController : Controller
    {
        private Model1Container db = new Model1Container();

        // GET: Accounts
        public ActionResult login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Autherize(tweett.Models.Account accountModel)
        {
            using (Model1Container db = new Model1Container())
            {
                var accountDetail = db.AccountSet.Where(x => x.username == accountModel.username && x.password == accountModel.password).FirstOrDefault();
                if(accountDetail==null)
                {
                    return View("Index", accountModel);
                }
                else
                {
                    Session["IdUser"] = accountDetail.IdUser;
                    return RedirectToAction("Index", "Home");
                }
                
            }
            
        }
       
        public ActionResult Profile()
        {
            return View(db.AccountSet.ToList());
        }
        //login
        public ActionResult login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Autherize(tweett.Models.Account accountModel)
        {
            using (Model1Container db = new Model1Container())
            {
                var accountDetail = db.AccountSet.Where(x => x.username == accountModel.username && x.password == accountModel.password).FirstOrDefault();
                if (accountDetail == null)
                {
                    return View("Index", accountModel);
                }
                else
                {
                    Session["IdUser"] = accountDetail.IdUser;
                    Session["Username"] = accountDetail.username;
                    return RedirectToAction("Index", "Posts");
                }

<<<<<<< HEAD
            }

        }
=======

>>>>>>> 8ca85eb950ffac674af1ea6bc81c5617634836b6
        // GET: Accounts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = db.AccountSet.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        // GET: Accounts/Create
        public ActionResult Create()
        {
            return View();
        }
       
       
        
        // POST: Accounts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
   
[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdUser,username,password,email")] Account account)
        {
            if (ModelState.IsValid)
            {
                db.AccountSet.Add(account);
                db.SaveChanges();
<<<<<<< HEAD
                return RedirectToAction("login");
=======
                return RedirectToAction("Create");
>>>>>>> 8ca85eb950ffac674af1ea6bc81c5617634836b6
            }

            return View(account);
        }

        // GET: Accounts/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = db.AccountSet.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        // POST: Accounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdUser,username,password,email")] Account account)
        {
            if (ModelState.IsValid)
            {
                db.Entry(account).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(account);
        }

        // GET: Accounts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = db.AccountSet.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        // POST: Accounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Account account = db.AccountSet.Find(id);
            db.AccountSet.Remove(account);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult  LogOut()
        {
            Session.Abandon();
            return RedirectToAction("", "Accounts");
        }

    }
}
