﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using tweett.Models;

namespace tweett.Controllers
{
    public class RepliesController : Controller
    {
        private Model1Container db = new Model1Container();

        // GET: Replies
        public ActionResult Index()         
        {
            if (Session["Username"] != null)
            {
                String user = Session["Username"].ToString();
                ViewData["User"] = db.AccountSet.FirstOrDefault(u => u.username == user);
            }
          
            return View(db.ReplySet.ToList());
        }

        // GET: Replies/Details/5
        public ActionResult Details(int? id)

        {
            ViewData["Posts"] = db.PostsSet.ToList();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reply reply = db.ReplySet.Find(id);
            List<Posts> repliess = db.PostsSet.ToList();
            ViewData["repliess"] = repliess;
            if (reply == null)
            {
                return HttpNotFound();
            }
           
            return View(reply);
        }

        // GET: Replies/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Replies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdReply,textR,date_time,PostIdPost,AccountIdUser")] Reply reply)
        {
            if (ModelState.IsValid)
            {
                db.ReplySet.Add(reply);
                db.SaveChanges();
                return RedirectToAction("Details","Posts", new { id = reply.IdReply });
            }

            return View(reply);
        }

        // GET: Replies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reply reply = db.ReplySet.Find(id);
            if (reply == null)
            {
                return HttpNotFound();
            }
            return View(reply);
        }

        // POST: Replies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdReply,textR,date_time,PostIdPost,AccountIdUser")] Reply reply)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reply).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details", "Posts");
            }
            return View(reply);
        }

        // GET: Replies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reply reply = db.ReplySet.Find(id);
            if (reply == null)
            {
                return HttpNotFound();
            }
            return View(reply);
        }

        // POST: Replies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reply reply = db.ReplySet.Find(id);
            db.ReplySet.Remove(reply);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
